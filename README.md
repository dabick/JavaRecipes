To allow debugging, add the following lines to startup.sh at the beginning:
set JPDA_ADDRESS=8000
set JPDA_TRANSPORT=dt_socket

Then to start the server use the command './catalina.sh jpda run'

NOTE: If running on windows, replace the .sh with .bat

Once the server is running it can be seen at http://localhost:8080/recipes/index.jsp

Will need to update Database connection information in resources.xml

Can now use the gradle task cargoRunLocal and cargoRedeployLocal

Will need to update TOMEE_ROOT/conf/context.xml to include within the <Context> element:
    
    <Manager className="org.apache.catalina.session.PersistentManager"
         maxIdleBackup="1"
         minIdleSwap="0"
         maxIdleSwap="0"
         processExpiresFrequency="1"
         saveOnRestart="true">

        <Store className="org.apache.catalina.session.JDBCStore"
               dataSourceName="recipes/jdbc/RecipeDb"
               localDataSource="true"
               sessionAppCol="app_name"
               sessionDataCol="session_data"
               sessionIdCol="session_id"
               sessionLastAccessedCol="last_access"
               sessionMaxInactiveCol="max_inactive"
               sessionTable="recipe_sessions"
               sessionValidCol="valid_session" />
    
    </Manager>

Also need to update TOMEE_ROOT/conf/catalina.properties to include the line:
    
    org.apache.catalina.STRICT_SERVLET_COMPLIANCE=true
