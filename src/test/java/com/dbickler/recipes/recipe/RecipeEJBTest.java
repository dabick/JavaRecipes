package com.dbickler.recipes.recipe;

import com.dbickler.recipes.db.DaoAdapter;
import com.dbickler.recipes.recipe.models.Recipe;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.TypedQuery;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Daniel Bickler
 */
@RunWith(MockitoJUnitRunner.class)
public class RecipeEJBTest {
    @Mock private DaoAdapter daoAdapter;

    private RecipeEJB ejb;

    @Before
    public void setUp() throws Exception {
        ejb = new RecipeEJB(daoAdapter);
    }

    @Test
    public void getRecipes() throws Exception {
        //noinspection unchecked
        TypedQuery<Recipe> query = mock(TypedQuery.class);
        when(daoAdapter.createNamedQuery(Recipe.GET_RECIPES, Recipe.class)).thenReturn(query);
        when(query.setMaxResults(10)).thenReturn(query);

        List<Recipe> list = new ArrayList<>();
        when(query.getResultList()).thenReturn(list);

        assertSame(list, ejb.getRecipes());
    }
}