package com.dbickler.recipes.security.permission;

import com.dbickler.recipes.db.DaoAdapter;
import com.dbickler.recipes.log.LoggerProvider;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

/**
 * Created by Daniel Bickler
 */
@RunWith(MockitoJUnitRunner.class)
public class PermissionsEJBTest {
    @Mock private DaoAdapter daoAdapter;
//    @Mock private WebApplicationExceptionHandler exceptionHandler;
    @Mock private Logger logger;
    @Mock private LoggerProvider loggerProvider;

//    private PermissionsEJB ejb;
//
//    private static final Set<Permission> EXPECTED_PERMISSIONS = Sets.newHashSet(
//            new WildcardPermission(PermissionDomain.BAN.toString().toLowerCase() + ":*:*:*"),
//            new WildcardPermission(PermissionDomain.USER.toString().toLowerCase() + ":*:4,3:5")
//    );
//
//    @Before
//    public void setUp() throws Exception {
//        when(loggerProvider.getLogger(PermissionsEJB.class)).thenReturn(logger);
//        ejb = new PermissionsEJB(daoAdapter, exceptionHandler, loggerProvider);
//    }
//
//    @Test
//    public void getPermissionsForUser() throws Exception {
//        Long userId = 1210432L;
//        RecipePermission permission1 = new RecipePermission(1L, userId, PermissionDomain.BAN.toString(), Sets.newHashSet("*"), Sets.newHashSet("*"));
//        RecipePermission permission2 = new RecipePermission(2L, userId, PermissionDomain.USER.toString(), Sets.newHashSet("*"), Sets.newHashSet("3", "4"), Sets.newHashSet("5"));
//
//        //noinspection unchecked
//        TypedQuery<RecipePermission> query = mock(TypedQuery.class);
//        when(daoAdapter.createNamedQuery(RecipePermission.GET_PERMISSIONS_BY_USER, RecipePermission.class))
//                .thenReturn(query);
//        when(query.setParameter(RecipePermission.USER_PARAMETER, userId)).thenReturn(query);
//        when(query.getResultList()).thenReturn(Lists.newArrayList(permission1, permission2));
//
//        Set<Permission> actual = ejb.getPermissionsForUser(userId);
//
//        assertTrue(actual.size() == EXPECTED_PERMISSIONS.size());
//        for (Permission permission : actual) {
//            if (permissionFails(permission)) {
//                fail(permission + " not matched.");
//            }
//        }
//    }
//
//    private boolean permissionFails(Permission permission) {
//        for (Permission expected : EXPECTED_PERMISSIONS) {
//            if (permission.implies(expected)) {
//                return false;
//            }
//        }
//        return true;
//    }
}