package com.dbickler.recipes.testutil;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Daniel Bickler
 */
public class TestRequestBuilder {
    private final HttpServletRequest request = mock(HttpServletRequest.class);

    private TestRequestBuilder() {}

    public static TestRequestBuilder newBuilder() {
        return new TestRequestBuilder();
    }

    public HttpServletRequest build() {
        return request;
    }

    public TestRequestBuilder withUsername(String username) {
        when(request.getParameter("username")).thenReturn(username);
        return this;
    }

    public TestRequestBuilder withEmail(String email) {
        when(request.getParameter("email")).thenReturn(email);
        return this;
    }

    public TestRequestBuilder withPassword(String password) {
        when(request.getParameter("password")).thenReturn(password);
        return this;
    }

    public TestRequestBuilder withConfirmPassword(String confirmPassword) {
        when(request.getParameter("confirmPassword")).thenReturn(confirmPassword);
        return this;
    }

    public TestRequestBuilder withFirstName(String firstName) {
        when(request.getParameter("firstName")).thenReturn(firstName);
        return this;
    }

    public TestRequestBuilder withLastName(String lastName) {
        when(request.getParameter("lastName")).thenReturn(lastName);
        return this;
    }
}
