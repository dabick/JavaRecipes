package com.dbickler.recipes.testutil;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import javax.ws.rs.WebApplicationException;
import java.util.Objects;

/**
 * Created by Daniel Bickler
 */
public class WebApplicationExceptionMatcher extends TypeSafeMatcher<WebApplicationException> {
    private final int status;
    private final String message;

    public WebApplicationExceptionMatcher(int status, String message) {
        this.status = status;
        this.message = message;
    }

    @Override
    protected boolean matchesSafely(WebApplicationException item) {
        return item.getResponse().getStatus() == status && Objects.equals(item.getResponse().getEntity(), message);
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue("Expected Status ").appendValue(status)
                .appendValue(" with message ").appendValue(message);
    }
}
