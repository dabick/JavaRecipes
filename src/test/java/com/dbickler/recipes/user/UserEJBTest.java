package com.dbickler.recipes.user;

import com.dbickler.recipes.db.DaoAdapter;
import com.dbickler.recipes.user.models.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.TypedQuery;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Daniel Bickler
 */
@RunWith(MockitoJUnitRunner.class)
public class UserEJBTest {
    @Mock private DaoAdapter daoAdapter;
//    @Mock private WebApplicationExceptionHandler exceptionHandler;
//    @Mock private BCryptPasswordService passwordService;
    @Mock private TypedQuery<User> query;
    @Mock private User user;
//    @Mock private PermissionsEJB permissionsEJB;

    private UserEJB ejb;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String EMAIL = "a@b.c";

    @Before
    public void setUp() throws Exception {
//        ejb = new UserEJB(daoAdapter, exceptionHandler, passwordService, permissionsEJB);
//
//        when(daoAdapter.createNamedQuery(User.GET_USER_BY_EMAIL, User.class)).thenReturn(query);
    }

    @Test
    public void getUserForEmail() throws Exception {
//        when(query.setParameter(User.EMAIL_PARAMETER, EMAIL)).thenReturn(query);
        when(query.getSingleResult()).thenReturn(user);

        assertEquals(user, ejb.getUserForEmail(EMAIL));
    }

    @Test
    public void testGetUserForEmailThrows() throws Exception {
//        when(query.setParameter(User.EMAIL_PARAMETER, EMAIL)).thenThrow(new NoResultException());
//
//        expectedException.expect(UnknownAccountException.class);

        ejb.getUserForEmail(EMAIL);
    }
}