import {AuthedUser} from '@/users/types';

const AUTH_USER_STORAGE_KEY = 'authUser';

export function storeLogin(authUser: AuthedUser) {
    window.localStorage.setItem(AUTH_USER_STORAGE_KEY, JSON.stringify(authUser));
}

export function checkLoggedIn(): AuthedUser | undefined {
    try {
        const possibleUser = window.localStorage.getItem(AUTH_USER_STORAGE_KEY);
        if (possibleUser) {
            return JSON.parse(possibleUser);
        }
    } catch (e) {
        console.log('Failed to check is logged in', e);
    }
}

export function unstoreLogin() {
    window.localStorage.removeItem(AUTH_USER_STORAGE_KEY);
}
