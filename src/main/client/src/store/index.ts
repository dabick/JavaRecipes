import Vue from 'vue';
import Vuex from 'vuex';
import recipes from '@/recipes/recipeStore';
import users from '@/users/userStore';
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        recipes,
        users,
    },
    strict: process.env.NODE_ENV !== 'production',
});
