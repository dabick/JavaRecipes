export class RecipeError extends Error {
    public displayError?: string;

    constructor(message: string, displayError?: string) {
        super(message);
        if (displayError) {
            this.displayError = displayError;
        }
    }
}

export function getError(err: any, defaultMsg: string): string {
    if (err instanceof RecipeError && err.displayError) {
        return err.displayError;
    }
    console.error(err);
    return defaultMsg;
}
