import {get, post} from '@/services/base';
import {Recipe, RecipeSummary} from '@/recipes/types';

export function getRecipeSummaries(): Promise<RecipeSummary[]> {
    return get('/recipes');
}

export function addRecipe(recipe: Recipe) {
    return post('/recipes', recipe);
}
