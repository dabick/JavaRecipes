import {makeRequest, post} from '@/services/base';
import {AuthedUser, SignupUser} from '@/users/types';

export function login(email: string, password: string): Promise<AuthedUser> {
    return makeRequest('/login', 'POST', { email, password }, true);
}

export function sendLogout(): Promise<void> {
    return post('/logout');
}

export function register(toCreate: SignupUser): Promise<AuthedUser> {
    return post('/users', toCreate);
}
