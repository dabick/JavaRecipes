import Vue from 'vue';
import {RecipeError} from '@/errors/RecipeError';

const isProduction = process.env.NODE_ENV === 'production';
const baseUrl = 'http://localhost:8081/api';

type Method = 'POST' | 'GET' | 'PUT' | 'DELETE';

export function get(path: string): Promise<any> {
    return makeRequest(path, 'GET');
}

export function post(path: string, body?: any): Promise<any> {
    return makeRequest(path, 'POST', body);
}

// `delete` is a reserved keyword so I'm sacrificing consistency to keep the other methods less verbose
export function httpDelete(path: string): Promise<any> {
    return makeRequest(path, 'DELETE');
}

export function put(path: string, body?: any): Promise<any> {
    return makeRequest(path, 'PUT', body);
}

export async function makeRequest(path: string, method: Method, data?: any, isLogin = false): Promise<any> {
    try {
        const response = await fetch(baseUrl + path, {
            method,
            mode: 'cors',
            credentials: isProduction ? 'same-origin' : 'include',
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            referrer: 'no-referrer',
            body: !data ? null : JSON.stringify(data),
        });
        if (response.ok) {
            if (response.status === 204) {
                return;
            } else {
                return response.json();
            }
        }
        throw response;
    } catch (err) {
        let message = 'An unknown error occurred';
        if (err.status) {
            switch (err.status) {
                case 400:
                    message = await err.text();
                    break;
                case 401:
                    message = isLogin ? 'Invalid email or password' : 'Your session has expired, Please login.';
                    (Vue as any).notify({
                        title: 'Unauthorized',
                        text: message,
                        type: isLogin ? 'error' : 'warn',
                    });
                    if (!isLogin) {
                        window.location.href = '/login';
                    }
                    return;
            }
        } else {
            console.error(err);
        }
        throw new RecipeError(message, message);
    }
}
