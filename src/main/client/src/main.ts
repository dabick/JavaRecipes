import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import Notifications from 'vue-notification';
import RecipesPlugin from '@/users/RecipesPlugin';

Vue.config.productionTip = false;

// Valid values for `type` are 'warn' (yellow), 'error' (red), 'success' (green). If blank (blue).
Vue.use(Notifications);
Vue.use(RecipesPlugin);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
