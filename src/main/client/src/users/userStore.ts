import {Module} from 'vuex';
import {AuthedUser} from '@/users/types';
import {login, sendLogout} from '@/services/login';
import {checkLoggedIn, storeLogin, unstoreLogin} from '@/storage';

export interface UserState {
    authUser?: AuthedUser;
}

const USER_MODULE = 'users';

export const IS_LOGGED_IN = 'isLoggedIn';
export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const SET_AUTH_USER = 'setAuthUser';

export function prefixUserModule(path: string): string {
    return USER_MODULE + '/' + path;
}

export default {
    namespaced: true,
    state: {
        authUser: undefined,
    },
    actions: {
        async [LOGIN]({ commit }, payload: {email: string, password: string}): Promise<void> {
            const user = await login(payload.email, payload.password);
            if (user) {
                commit(SET_AUTH_USER, user);
            }
        },
        async [LOGOUT]({ commit }): Promise<void> {
            await sendLogout();
            unstoreLogin();
            commit(SET_AUTH_USER);
        },
        [IS_LOGGED_IN]({commit, state}) {
            if (!state.authUser) {
                const user = checkLoggedIn();
                if (user) {
                    commit(SET_AUTH_USER, user);
                }
            }
            return Boolean(state.authUser);
        },
    },
    mutations: {
        [SET_AUTH_USER](state, user?: AuthedUser) {
            state.authUser = user;
            if (user) {
                storeLogin(user);
            }
        },
    },
    getters: {
    },
} as Module<UserState, any>;
