import Vue, {VueConstructor} from 'vue';
import {IS_LOGGED_IN, prefixUserModule} from '@/users/userStore';
import store from '@/store';
import {getError} from '@/errors/RecipeError';

export default {
    install(vue: VueConstructor<Vue>) {
        vue.mixin({
            computed: {
                isLoggedIn(): boolean {
                    return Boolean(store.state.users.authUser);
                },
            },
            created() {
                (this as any).$isLoggedIn();
            },
            methods: {
                $isLoggedIn() {
                    return store.dispatch(prefixUserModule(IS_LOGGED_IN));
                },
                $notifyError(err: any, defaultMessage: string) {
                    this.$notify({
                        title: 'Error',
                        text: getError(err, defaultMessage),
                        type: 'error',
                    });
                },
                $notifySuccess(text: string, title = 'Success') {
                    this.$notify({
                        title,
                        text,
                        type: 'success',
                    });
                },
            },
        });
    },
};
