export interface AuthedUser {
    id: number;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
}

export class SignupUser {
    public userName = '';
    public firstName = '';
    public lastName = '';
    public email = '';
    public password = '';
    public confirmPassword = '';
}
