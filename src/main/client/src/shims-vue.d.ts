// noinspection ES6UnusedImports This is needed
import Vue from 'vue';

declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

//LoggedInPlugin
declare module 'vue/types/vue' {
    interface Vue {
        isLoggedIn: boolean;
        $isLoggedIn: () => boolean;
        $notifyError: (err: any, defaultMessage: string) => void;
        $notifySuccess: (text: string, title?: string) => void;
    }
}
