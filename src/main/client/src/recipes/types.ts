export interface RecipeSummary {
    id: number;
    name: string;
    description: string;
    submittedBy: string;
}

export class Recipe {
    public id = 0;
    public name = '';
    public description = '';
    public originallyBy = '';
    public ingredients: RecipeItem[] = [];
    public instructions: RecipeItem[] = [];
}

/*
 * Until/Unless they diverge Instructions and Ingredients will be stored here.
 */
export interface RecipeItem {
    recipeId: number;
    name: string;
}
