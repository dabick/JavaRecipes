import {Module} from 'vuex';
import {RecipeSummary} from '@/recipes/types';
import {getRecipeSummaries} from '@/services/recipes';

export interface RecipeState {
    recipeSummaries: RecipeSummary[];
    loadingSummaries: boolean;
}

export const MODULE = 'recipes';

const SET_SUMMARIES = 'setSummaries';
const SET_LOADING_SUMMARIES = 'setLoadingSummaries';

export const LOAD_SUMMARIES = 'loadSummaries';

export function prefixRecipesModule(path: string): string {
    return MODULE + '/' + path;
}

export default {
    namespaced: true,
    state: {
        recipeSummaries: [],
        loadingSummaries: false,
    },
    actions: {
        async [LOAD_SUMMARIES]({ commit }) {
            commit(SET_LOADING_SUMMARIES, true);
            commit(SET_SUMMARIES, await getRecipeSummaries());
            commit(SET_LOADING_SUMMARIES, false);
        },
    },
    mutations: {
        [SET_SUMMARIES](state, recipeSummaries) {
            state.recipeSummaries = recipeSummaries;
        },
        [SET_LOADING_SUMMARIES](state, loading: boolean) {
            state.loadingSummaries = loading;
        },
    },
} as Module<RecipeState, any>;
