package com.dbickler.recipes.translation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Daniel Bickler
 */
public class ListTranslator {
    public static <JSON_MODEL, JPA_MODEL> List<JSON_MODEL> fromJpa(Collection<JPA_MODEL> jpaModels, Function<JPA_MODEL, JSON_MODEL> function) {
        if (jpaModels == null) {
            return new ArrayList<>();
        }
        return jpaModels.stream().map(function).collect(Collectors.toList());
    }

    public static <JSON_MODEL, JPA_MODEL> List<JPA_MODEL> toJpa(Collection<JSON_MODEL> jsonModels, Function<JSON_MODEL, JPA_MODEL> function) {
        if (jsonModels == null) {
            return new ArrayList<>();
        }
        return jsonModels.stream().map(function).collect(Collectors.toList());
    }
}
