package com.dbickler.recipes.user;

import com.dbickler.recipes.db.Parameter;
import com.dbickler.recipes.db.ParameterBuilder;
import com.dbickler.recipes.db.models.DbModel;
import com.dbickler.recipes.user.models.User;

import java.util.List;

/**
 * @author Daniel Bickler
 */
public class UserDb implements DbModel<User> {
    private final User user;

    UserDb(User user) {
        this.user = user;
    }

    @Override
    public String getInsertSql() {
        return " INSERT INTO users " +
                " (username, email, confirmed_email, password, first_name, last_name, in_beta, requested_beta, admin, moderator, editor) " +
                " VALUES " +
                " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id ";
    }

    @Override
    public List<Parameter<?>> getParametersForInsert() {
        return ParameterBuilder.create()
                .addString(user.getUsername())
                .addString(user.getEmail())
                .addString(user.getConfirmedEmail())
                .addString(user.getPassword())
                .addString(user.getFirstName())
                .addString(user.getLastName())
                .addBoolean(false)
                .addBoolean(false)
                .addBoolean(false)
                .addBoolean(false)
                .addBoolean(false)
                .build();
    }

    @Override
    public String getUpdateSql() {
        return "UPDATE users SET email = ?, first_name = ?, last_name = ?, password = ? WHERE id = ?";
    }

    @Override
    public List<Parameter<?>> getParametersForUpdate() {
        return ParameterBuilder.create()
                .addString(user.getEmail())
                .addString(user.getFirstName())
                .addString(user.getLastName())
                .addString(user.getPassword())
                .addLong(user.getId())
                .build();
    }

    @Override
    public User data() {
        return user;
    }
}
