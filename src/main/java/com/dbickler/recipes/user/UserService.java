package com.dbickler.recipes.user;

import com.dbickler.recipes.errors.exceptions.ValidationException;
import com.dbickler.recipes.security.Secured;
import com.dbickler.recipes.security.session.SessionUtil;
import com.dbickler.recipes.user.models.User;
import com.dbickler.recipes.user.models.json.SignupUser;
import com.dbickler.recipes.user.models.json.UpdateUserRequest;
import com.dbickler.recipes.user.models.json.UserModel;
import com.google.common.base.Strings;
import com.goterl.lazycode.lazysodium.LazySodiumJava;
import com.goterl.lazycode.lazysodium.exceptions.SodiumException;
import com.goterl.lazycode.lazysodium.interfaces.PwHash;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Daniel Bickler
 */
@Path("/users")
public class UserService {
    private UserEJB userEJB;
    private LazySodiumJava lazySodium;
    private SessionUtil sessionUtil;

    @Inject
    @SuppressWarnings("unused")
    void init(UserEJB userEJB, LazySodiumJava lazySodium, SessionUtil sessionUtil) {
        this.userEJB = userEJB;
        this.lazySodium = lazySodium;
        this.sessionUtil = sessionUtil;
    }

    @GET
    @Secured
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserModel getUser(@PathParam("id") Long id) {
        User user = userEJB.getUserForId(id);
        if (user == null) {
            throw new ValidationException("No user found for " + id);
        }
        return new UserModel(user);
    }

    @PUT
    @Secured
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateUser(UpdateUserRequest user) {
        try {
            validate(user);
            User currentUser = userEJB.getUserForId(user.getId());
            if (currentUser == null) {
                throw new ValidationException("No user found for " + user.getId());
            }
            User newUser = currentUser.builder()
                    .withEmail(user.getEmail())
                    .withFirstName(user.getFirstName())
                    .withLastName(user.getLastName())
                    .build();
            userEJB.update(newUser, user.getPasswordChangeRequest());
        } catch (SodiumException e) {
            throw new RuntimeException(e);
        }
    }

    private void validate(UpdateUserRequest user) {
        if (user == null || user.getId() == null || user.getId() <= 0) {
            throw new ValidationException("User/ID is required.");
        }
    }

    @POST
    public UserModel signup(SignupUser userModel, @Context HttpServletRequest httpRequest) {
        String password = preparePassword(userModel.getPassword(), userModel.getConfirmPassword());
        User user = new User(null, userModel.getUserName(), userModel.getEmail(), password, userModel.getFirstName(), userModel.getLastName());
        user = userEJB.insert(user);
        sessionUtil.login(httpRequest, user);
        return new UserModel(user);
    }

    private String preparePassword(String password, String confirmPassword) {
        if (Strings.isNullOrEmpty(password)) {
            throw ValidationException.buildRequired("Password");
        } else if (!password.equals(confirmPassword)) {
            throw new ValidationException("Passwords don't match.");
        }
        try {
            return lazySodium.cryptoPwHashStr(password, PwHash.ARGON2ID_OPSLIMIT_MIN, PwHash.ARGON2ID_MEMLIMIT_MIN);
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }
}
