package com.dbickler.recipes.user.models.json;

import com.dbickler.recipes.user.models.PasswordChangeRequest;

import java.util.Objects;

/**
 * Created by Daniel Bickler
 */
public class UpdateUserRequest {
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String confirmPassword;
    private String oldPassword;

    //For JSON serialization
    private UpdateUserRequest() {}

    public UpdateUserRequest(Long id, String email, String firstName, String lastName, String password, String confirmPassword, String oldPassword) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.oldPassword = oldPassword;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public PasswordChangeRequest getPasswordChangeRequest() {
        return new PasswordChangeRequest(password, confirmPassword, oldPassword);
    }

    @Override
    public String toString() {
        return "UpdateUserRequest{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", oldPassword='" + oldPassword + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateUserRequest that = (UpdateUserRequest) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(email, that.email) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(password, that.password) &&
                Objects.equals(confirmPassword, that.confirmPassword) &&
                Objects.equals(oldPassword, that.oldPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, firstName, lastName, password, confirmPassword, oldPassword);
    }
}
