package com.dbickler.recipes.user.models.json;

import com.dbickler.recipes.user.models.User;

/**
 * @author Daniel Bickler
 */
public class UserModel {
    private Long id;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;

    @Deprecated public UserModel() {} //For JAX-RS

    public UserModel(User user) {
        id = user.getId();
        userName = user.getUsername();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        email = user.getEmail();
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }
}
