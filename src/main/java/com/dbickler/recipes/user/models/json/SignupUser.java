package com.dbickler.recipes.user.models.json;

/**
 * @author Daniel Bickler
 */
public class SignupUser extends UserModel {
    private String password;
    private String confirmPassword;

    @Deprecated public SignupUser() {} //For JAX-RS

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }
}
