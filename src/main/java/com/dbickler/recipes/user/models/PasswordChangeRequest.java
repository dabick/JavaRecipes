package com.dbickler.recipes.user.models;

import com.google.common.base.Strings;

import java.util.Objects;

/**
 * Created by Daniel Bickler
 */
public class PasswordChangeRequest {
    private final String password;
    private final String confirmPassword;
    private final String oldPassword;

    public PasswordChangeRequest(String password, String confirmPassword, String oldPassword) {
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.oldPassword = oldPassword;
    }

    public boolean isRequested() {
        return present(password) || present(confirmPassword) || present(oldPassword);
    }

    private boolean present(String str) {
        return !Strings.isNullOrEmpty(str);
    }

    public String isInvalid() {
        if (!present(password)) {
            return "Password is required.";
        } if (!present(confirmPassword)) {
            return "Confirm Password is required.";
        } else if (!password.equals(confirmPassword)) {
            return "New and Confirm Passwords don't match";
        } else if (!present(oldPassword)) {
            return "Old Password is required.";
        }
        return "";
    }

    public String getPassword() {
        return password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    @Override
    public String toString() {
        return "PasswordChangeRequest{" +
                "password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", oldPassword='" + oldPassword + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasswordChangeRequest that = (PasswordChangeRequest) o;
        return Objects.equals(password, that.password) &&
                Objects.equals(confirmPassword, that.confirmPassword) &&
                Objects.equals(oldPassword, that.oldPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(password, confirmPassword, oldPassword);
    }
}
