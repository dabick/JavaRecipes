package com.dbickler.recipes.user.models;

import com.dbickler.recipes.db.models.HasBuilder;
import com.dbickler.recipes.models.HasId;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Created by Daniel Bickler
 */
public class User implements HasId, Serializable, HasBuilder<User> {
    private final Long id;
    private final String username;
    private final String email;
    private final String confirmedEmail;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final Boolean inBeta;
    private final Boolean requestedBeta;
    private final Boolean admin;
    private final Boolean moderator;
    private final Boolean editor;

    public User(Long id, String username, String email, String password, String firstName, String lastName) {
        this(id, username, email, password, firstName, lastName, false, false, false, false, false);
    }

    public User(Long id, String username, String email, String password, String firstName, String lastName,
                Boolean inBeta, Boolean requestedBeta, Boolean admin, Boolean moderator, Boolean editor) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.confirmedEmail = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.inBeta = inBeta;
        this.requestedBeta = requestedBeta;
        this.admin = admin;
        this.moderator = moderator;
        this.editor = editor;
    }

    public User(ResultSet rs) throws SQLException {
        id = rs.getLong("id");
        username = rs.getString("username");
        email = rs.getString("email");
        confirmedEmail = rs.getString("confirmed_email");
        password = rs.getString("password");
        firstName = rs.getString("first_name");
        lastName = rs.getString("last_name");
        inBeta = rs.getBoolean("in_beta");
        requestedBeta = rs.getBoolean("requested_beta");
        admin = rs.getBoolean("admin");
        moderator = rs.getBoolean("moderator");
        editor = rs.getBoolean("editor");
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getConfirmedEmail() {
        return confirmedEmail;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Boolean getInBeta() {
        return inBeta;
    }

    public Boolean getRequestedBeta() {
        return requestedBeta;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public Boolean getModerator() {
        return moderator;
    }

    public Boolean getEditor() {
        return editor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username) &&
                Objects.equals(email, user.email) &&
                Objects.equals(confirmedEmail, user.confirmedEmail) &&
                Objects.equals(password, user.password) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(inBeta, user.inBeta) &&
                Objects.equals(requestedBeta, user.requestedBeta) &&
                Objects.equals(admin, user.admin) &&
                Objects.equals(moderator, user.moderator) &&
                Objects.equals(editor, user.editor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, email, confirmedEmail, password, firstName, lastName, inBeta, requestedBeta, admin, moderator, editor);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", confirmedEmail='" + confirmedEmail + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", inBeta=" + inBeta +
                ", requestedBeta=" + requestedBeta +
                ", admin=" + admin +
                ", moderator=" + moderator +
                ", editor=" + editor +
                '}';
    }

    @Override
    public Builder builder() {
        return new Builder(id, username, email, password, firstName, lastName, inBeta, requestedBeta, admin, moderator, editor);
    }

    public static class Builder implements com.dbickler.recipes.db.models.Builder<User> {
        private Long id;
        private String username;
        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private Boolean inBeta;
        private Boolean requestedBeta;
        private Boolean admin;
        private Boolean moderator;
        private Boolean editor;

        public Builder() {
        }

        public Builder(Long id, String username, String email, String password, String firstName, String lastName,
                       Boolean inBeta, Boolean requestedBeta, Boolean admin, Boolean moderator, Boolean editor) {
            this.id = id;
            this.username = username;
            this.email = email;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.inBeta = inBeta;
            this.requestedBeta = requestedBeta;
            this.admin = admin;
            this.moderator = moderator;
            this.editor = editor;
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withInBeta(Boolean inBeta) {
            this.inBeta = inBeta;
            return this;
        }

        public Builder withRequestedBeta(Boolean requestedBeta) {
            this.requestedBeta = requestedBeta;
            return this;
        }

        public Builder withAdmin(Boolean admin) {
            this.admin = admin;
            return this;
        }

        public Builder withModerator(Boolean moderator) {
            this.moderator = moderator;
            return this;
        }

        public Builder withEditor(Boolean editor) {
            this.editor = editor;
            return this;
        }

        public User build() {
            return new User(id, username, email, password, firstName, lastName, inBeta, requestedBeta, admin, moderator, editor);
        }
    }
}
