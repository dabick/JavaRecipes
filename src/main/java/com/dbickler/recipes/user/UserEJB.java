package com.dbickler.recipes.user;

import com.dbickler.recipes.db.DaoAdapter;
import com.dbickler.recipes.db.ParameterBuilder;
import com.dbickler.recipes.errors.exceptions.ValidationException;
import com.dbickler.recipes.user.models.PasswordChangeRequest;
import com.dbickler.recipes.user.models.User;
import com.google.common.base.Strings;
import com.goterl.lazycode.lazysodium.LazySodiumJava;
import com.goterl.lazycode.lazysodium.exceptions.SodiumException;
import com.goterl.lazycode.lazysodium.interfaces.PwHash;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Optional;

/**
 * Created by Daniel Bickler
 */
@LocalBean
@Stateless
public class UserEJB {
    private static final String GET_USER =
            " SELECT id, username, email, confirmed_email, password, first_name, last_name, in_beta, requested_beta, admin, moderator, editor FROM users ";

    private final DaoAdapter daoAdapter;
    private final LazySodiumJava lazySodium;

    @Inject
    UserEJB(DaoAdapter daoAdapter,
            LazySodiumJava lazySodium) {
        this.daoAdapter = daoAdapter;
        this.lazySodium = lazySodium;
    }

    public User getUserForId(Long id) {
        if (id == null || id <= 0) {
            throw new ValidationException("Id is required.");
        }
        return getUser("id", ParameterBuilder.create().addLong(id))
                .orElseThrow(() -> new ValidationException("Unable to find user for " + id));
    }

    private Optional<User> getUser(String field, ParameterBuilder builder) {
        return daoAdapter.getOne(GET_USER + " WHERE " + field + " = ? ", builder.build(), User::new);
    }

    public Optional<User> getUserForEmail(String email) {
        return getUser("confirmed_email", ParameterBuilder.create().addString(email));
    }

    public void update(User user, PasswordChangeRequest passwordChangeRequest) throws SodiumException {
        validateUser(user);
        User oldUser = getUserForId(user.getId());
        checkEmail(user, oldUser);
        user = user.builder().withPassword(getNewPassword(passwordChangeRequest, oldUser)).build();
        daoAdapter.update(new UserDb(user));
    }

    private void validateUser(User user) {
        if (Strings.isNullOrEmpty(user.getPassword())) {
            throw ValidationException.buildRequired("Password");
        } else if (Strings.isNullOrEmpty(user.getUsername())) {
            throw ValidationException.buildRequired("Username");
        } else if (Strings.isNullOrEmpty(user.getEmail())) {
            throw ValidationException.buildRequired("Email");
        }
    }

    private void checkEmail(User newUser, User oldUser) {
        if (!newUser.getEmail().equals(oldUser.getEmail())) {
            //todo for now do nothing. Someday will send confirmation email and make a "pending Email" switch.
        }
    }

    private String getNewPassword(PasswordChangeRequest changeRequest, User oldUser) throws SodiumException {
        if (!changeRequest.isRequested()) {
            return oldUser.getPassword();
        }
        String message = changeRequest.isInvalid();
        if (!message.isEmpty()) {
            throw new ValidationException(message);
        } else if (!lazySodium.cryptoPwHashStrVerify(oldUser.getPassword(), changeRequest.getOldPassword())) {
            throw new ValidationException("Old password doesn't match current password.");
        }
        return lazySodium.cryptoPwHashStr(changeRequest.getPassword(), PwHash.ARGON2ID_OPSLIMIT_MIN, PwHash.ARGON2ID_MEMLIMIT_MIN);
    }

    /**
     * @param user to insert. NOTE: The password set on this object *MUST* already be hashed.
     */
    public User insert(User user) {
        validateForInsert(user);
        return daoAdapter.insert(new UserDb(user));
//        permissionsEJB.insertDefaultPermissions(user.getId());
    }

    private void validateForInsert(User user) {
        if (user == null) {
            throw ValidationException.buildRequired("User");
        } else if (user.getId() != null) {
            throw new ValidationException("Id must not be populated.");
        }
        validateUser(user);
    }
}
