package com.dbickler.recipes.db;

/**
 * @author Daniel Bickler
 */
public class Parameter<T> {
    final ParameterType<T> type;
    final T data;

    Parameter(ParameterType<T> type, T data) {
        this.type = type;
        this.data = data;
    }
}
