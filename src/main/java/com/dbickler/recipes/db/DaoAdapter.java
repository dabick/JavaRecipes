package com.dbickler.recipes.db;

import com.dbickler.recipes.db.models.DbModel;
import com.dbickler.recipes.db.models.HasBuilder;
import com.dbickler.recipes.functional.HandlingFunction;
import com.dbickler.recipes.models.HasId;

import javax.inject.Inject;
import javax.inject.Provider;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Facade around JPA. Also assists in Unit Testing JPA.
 *
 * Created by Daniel Bickler
 */
public class DaoAdapter {
    private final Provider<Connection> connection;

    @Inject
    DaoAdapter(Provider<Connection> connection) {
        this.connection = connection;
    }

    public <T> List<T> getAny(String sql, List<Parameter<?>> parameters, HandlingFunction<ResultSet, T> translator) {
        try (Connection conn = connection.get(); PreparedStatement ps = conn.prepareStatement(sql)) {
            setParameters(ps, parameters);
            try (ResultSet rs = ps.executeQuery()) {
                List<T> list = new ArrayList<>();
                while (rs.next()) {
                    list.add(translator.apply(rs));
                }
                return list;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public <T extends HasId> Optional<T> getOne(String sql, List<Parameter<?>> parameters, HandlingFunction<ResultSet, T> translator) {
        return this.get(sql, parameters, translator).values().stream().findFirst();
    }

    public <T extends HasId> Map<Long, T> get(String sql, List<Parameter<?>> parameters, HandlingFunction<ResultSet, T> translator) {
        try (Connection conn = connection.get(); PreparedStatement ps = conn.prepareStatement(sql)) {
            setParameters(ps, parameters);
            try (ResultSet rs = ps.executeQuery()) {
                return translate(rs, translator);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public <T extends HasId & HasBuilder<T>> T insert(DbModel<T> model) {
        Long newId = insert(model.getInsertSql(), model.getParametersForInsert());
        return model.data().builder().withId(newId).build();
    }

    public Long insert(String sql, List<Parameter<?>> parameters) {
        try (Connection conn = connection.get(); PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            setParameters(ps, parameters);
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Insert failed. No rows affected");
            }
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getLong(1);
                } else {
                    throw new SQLException("No IDs returned.");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public <T extends HasId & HasBuilder<T>> int update(DbModel<T> model) {
        return update(model.getUpdateSql(), model.getParametersForUpdate());
    }

    public int update(String sql, List<Parameter<?>> parameters) {
        try (Connection conn = connection.get(); PreparedStatement ps = conn.prepareStatement(sql)) {
            setParameters(ps, parameters);
            return ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void setParameters(PreparedStatement ps, List<Parameter<?>> parameters) throws SQLException {
        int i = 1;
        for (Parameter parameter : parameters) {
            setParameter(ps, parameter, i);
            i += 1;
        }
    }

    private void setParameter(PreparedStatement ps, Parameter<?> parameter, int i) throws SQLException {
        if (parameter.type == ParameterType.LONG) {
            ps.setLong(i, (Long) parameter.data);
        } else if (parameter.type == ParameterType.STRING) {
            ps.setString(i, (String) parameter.data);
        } else if (parameter.type == ParameterType.BIG_DECIMAL) {
            ps.setBigDecimal(i, (BigDecimal)parameter.data);
        } else if (parameter.type == ParameterType.DATE) {
            ps.setDate(i, (Date) parameter.data);
        } else if (parameter.type == ParameterType.BOOLEAN) {
            ps.setBoolean(i, (Boolean) parameter.data);
        } else if (parameter.type == ParameterType.SHORT) {
            ps.setShort(i, (Short)parameter.data);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private <T extends HasId> Map<Long, T> translate(ResultSet rs, HandlingFunction<ResultSet, T> translator) throws SQLException {
        Map<Long, T> results = new HashMap<>();
        while (rs.next()) {
            T model = translator.apply(rs);
            results.put(model.getId(), model);
        }
        return results;
    }
}
