package com.dbickler.recipes.db;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author Daniel Bickler
 */
interface ParameterType<T> {
    ParameterType<String> STRING = new ParameterType<>() {};
    ParameterType<byte[]> BYTES = new ParameterType<>() {};
    ParameterType<Long> LONG = new ParameterType<>() {};
    ParameterType<Short> SHORT = new ParameterType<>() {};
    ParameterType<BigDecimal> BIG_DECIMAL = new ParameterType<>() {};
    ParameterType<Date> DATE = new ParameterType<>() {};
    ParameterType<Boolean> BOOLEAN = new ParameterType<>() {};
}
