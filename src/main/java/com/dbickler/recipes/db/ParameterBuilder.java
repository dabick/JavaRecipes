package com.dbickler.recipes.db;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Bickler
 */
public final class ParameterBuilder {
    private final List<Parameter<?>> parameters = new ArrayList<>();

    private ParameterBuilder() {}

    public static ParameterBuilder create() {
        return new ParameterBuilder();
    }

    public ParameterBuilder addString(String s) {
        parameters.add(new Parameter<>(ParameterType.STRING, s));
        return this;
    }

    public ParameterBuilder addBytes(byte[] s) {
        parameters.add(new Parameter<>(ParameterType.BYTES, s));
        return this;
    }

    public ParameterBuilder addLong(Long s) {
        parameters.add(new Parameter<>(ParameterType.LONG, s));
        return this;
    }

    public ParameterBuilder addShort(Short s) {
        parameters.add(new Parameter<>(ParameterType.SHORT, s));
        return this;
    }

    public ParameterBuilder addLongs(Iterable<Long> longs) {
        longs.forEach(aLong -> parameters.add(new Parameter<>(ParameterType.LONG, aLong)));
        return this;
    }

    public ParameterBuilder addBigDecimal(BigDecimal bd) {
        parameters.add(new Parameter<>(ParameterType.BIG_DECIMAL, bd));
        return this;
    }

    public ParameterBuilder addDate(LocalDate date) {
        parameters.add(new Parameter<>(ParameterType.DATE, Date.valueOf(date)));
        return this;
    }

    public ParameterBuilder addBoolean(Boolean b) {
        parameters.add(new Parameter<>(ParameterType.BOOLEAN, b));
        return this;
    }

    public List<Parameter<?>> build() {
        return new ArrayList<>(parameters);
    }
}
