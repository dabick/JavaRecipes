package com.dbickler.recipes.db.models;

import com.dbickler.recipes.db.Parameter;
import com.dbickler.recipes.models.HasId;

import java.util.List;

/**
 * @author Daniel Bickler
 */
public interface DbModel<T extends HasId & HasBuilder<T>> {
    String getInsertSql();
    List<Parameter<?>> getParametersForInsert();
    String getUpdateSql();
    List<Parameter<?>> getParametersForUpdate();
    T data();
}
