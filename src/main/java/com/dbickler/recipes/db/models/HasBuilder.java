package com.dbickler.recipes.db.models;

import com.dbickler.recipes.models.HasId;

/**
 * @author Daniel Bickler
 */
public interface HasBuilder<T extends HasId & HasBuilder<T>> {
    Builder<T> builder();
}
