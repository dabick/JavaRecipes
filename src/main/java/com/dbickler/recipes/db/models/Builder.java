package com.dbickler.recipes.db.models;

import com.dbickler.recipes.models.HasId;

/**
 * @author Daniel Bickler
 */
public interface Builder<T extends HasId & HasBuilder<T>> {
    Builder<T> withId(Long id);
    T build();
}
