package com.dbickler.recipes.db;

import javax.enterprise.inject.Produces;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * "Module" to provide Database related objects.
 *
 * Created by Daniel Bickler
 */
@SuppressWarnings("unused")
public class DatabaseModule {

    @Produces
    DataSource produceDataSource() {
        try {
            return (DataSource) InitialContext.doLookup("java:comp/env/recipes/jdbc/RecipeDb");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Note with using this, it must always be wrapped in a {@link javax.enterprise.inject.Instance}
     * to ensure a fresh and open connection is provided.
     *
     * It is the injecting class's job to close the connection.
     */
    @Produces
    Connection produceConnection(DataSource ds) {
        try {
            return ds.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
