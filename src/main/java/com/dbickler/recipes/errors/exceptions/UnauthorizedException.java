package com.dbickler.recipes.errors.exceptions;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;

/**
 * Convenience Wrapper around {@link NotAuthorizedException}.
 * Handled by ClientErrorMapper.
 *
 * @author Daniel Bickler
 */
public class UnauthorizedException extends NotAuthorizedException {
    public UnauthorizedException() {
        super(Response.status(Response.Status.UNAUTHORIZED));
    }
}
