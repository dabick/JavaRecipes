package com.dbickler.recipes.errors.exceptions;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;

/**
 * @author Daniel Bickler
 */
public class ValidationException extends BadRequestException {
    private static final String IS_REQUIRED_MESSAGE = "%s is required.";

    public ValidationException(String message) {
        super(Response.status(Response.Status.BAD_REQUEST).entity(message).build());
    }

    public static ValidationException buildRequired(String entity) {
        return new ValidationException(String.format(IS_REQUIRED_MESSAGE, entity));
    }
}
