package com.dbickler.recipes.errors.mappers;

import javax.json.stream.JsonParsingException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Daniel Bickler
 */
@Provider
public class JsonExceptionMapper implements ExceptionMapper<JsonParsingException> {
    @Override
    public Response toResponse(JsonParsingException e) {
        return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
    }
}
