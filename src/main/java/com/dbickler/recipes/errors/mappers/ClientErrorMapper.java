package com.dbickler.recipes.errors.mappers;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * This mapper handles any Exceptions extending from JAX-RS's {@link ClientErrorException}.
 *
 * @author Daniel Bickler
 */
@Provider
public class ClientErrorMapper implements ExceptionMapper<ClientErrorException> {
        @Override
        public Response toResponse(ClientErrorException e) {
            return Response.status(e.getResponse().getStatus()).entity(e.getResponse().getEntity()).build();
        }
}
