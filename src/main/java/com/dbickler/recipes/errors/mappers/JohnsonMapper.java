package com.dbickler.recipes.errors.mappers;

import org.apache.johnzon.mapper.MapperException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Daniel Bickler
 */
@Provider
public class JohnsonMapper implements ExceptionMapper<MapperException> {
    @Override
    public Response toResponse(MapperException e) {
        return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
    }
}
