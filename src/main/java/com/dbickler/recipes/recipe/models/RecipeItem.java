package com.dbickler.recipes.recipe.models;

import java.util.Objects;

/**
 * Created by Daniel Bickler
 */
public class RecipeItem {
    private final Long recipeId;
    private final Short sequence;
    private final String name;

    public RecipeItem(Long recipeId, Short sequence, String name) {
        this.recipeId = recipeId;
        this.sequence = sequence;
        this.name = name;
    }

    public RecipeItem(RecipeItemModel item) {
        this.recipeId = item.getRecipeId();
        this.sequence = 0;
        this.name = item.getName();
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public Short getSequence() {
        return sequence;
    }

    public String getName() {
        return name;
    }

    public Builder builder() {
        return new Builder(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecipeItem that = (RecipeItem) o;
        return Objects.equals(recipeId, that.recipeId) &&
                Objects.equals(sequence, that.sequence) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeId, sequence, name);
    }

    @Override
    public String toString() {
        return "RecipeItem{" +
                "recipeId=" + recipeId +
                ", sequence=" + sequence +
                ", name='" + name + '\'' +
                '}';
    }

    public static class Builder {
        private Long recipeId;
        private Short sequence;
        private String name;

        public Builder(Long recipeId, Short sequence, String name) {
            this.recipeId = recipeId;
            this.sequence = sequence;
            this.name = name;
        }

        public Builder(RecipeItem item) {
            recipeId = item.recipeId;
            sequence = item.sequence;
            name = item.name;
        }

        public Long getRecipeId() {
            return recipeId;
        }

        public Builder withRecipeId(Long recipeId) {
            this.recipeId = recipeId;
            return this;
        }

        public Short getSequence() {
            return sequence;
        }

        public Builder withSequence(Short sequence) {
            this.sequence = sequence;
            return this;
        }

        public String getName() {
            return name;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public RecipeItem build() {
            return new RecipeItem(recipeId, sequence, name);
        }
    }
}
