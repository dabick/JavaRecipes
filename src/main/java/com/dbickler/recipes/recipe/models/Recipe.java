package com.dbickler.recipes.recipe.models;

import com.dbickler.recipes.db.models.HasBuilder;
import com.dbickler.recipes.models.HasId;
import com.dbickler.recipes.translation.ListTranslator;
import com.google.common.base.Strings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Daniel Bickler
 */
public class Recipe implements HasId, HasBuilder<Recipe> {
    private static final int SUMMARY_LIMIT = 150;

    private final Long id;
    private final Long submitterId;
    private final Long groupId;
    private final String originallyBy;
    private final LocalDate submittedOn;
    private final String name;
    private final String description;
    private final List<RecipeItem> instructions;
    private final List<RecipeItem> ingredients;

    public Recipe(Long id, Long submitterId, Long groupId, String originallyBy, LocalDate submittedOn,
                  String name, String description, List<RecipeItem> instructions, List<RecipeItem> ingredients) {
        this.id = id;
        this.submitterId = submitterId;
        this.groupId = groupId;
        this.originallyBy = originallyBy;
        this.submittedOn = submittedOn;
        this.name = name;
        this.description = description;
        this.instructions = instructions;
        this.ingredients = ingredients;
    }

    public Recipe(RecipeModel recipe) {
        id = recipe.getId();
        submitterId = recipe.getSubmitterId();
        groupId = recipe.getGroupId();
        originallyBy = recipe.getOriginallyBy();
        submittedOn = Strings.isNullOrEmpty(recipe.getSubmittedOn()) ? LocalDate.now() : LocalDate.parse(recipe.getSubmittedOn());
        name = recipe.getName();
        description = recipe.getDescription();
        instructions = ListTranslator.toJpa(recipe.getInstructions(), RecipeItem::new);
        ingredients = ListTranslator.toJpa(recipe.getIngredients(), RecipeItem::new);
    }

    public Recipe(ResultSet rs) throws SQLException {
        id = rs.getLong("id");
        submitterId = rs.getLong("submitter_id");
        groupId = rs.getLong("group_id");
        originallyBy = rs.getString("originally_by");
        submittedOn = rs.getDate("submitted_on").toLocalDate();
        name = rs.getString("name");
        description = rs.getString("description");
        instructions = new ArrayList<>();
        ingredients = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return id;
    }

    public Long getSubmitterId() {
        return submitterId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public String getOriginallyBy() {
        return originallyBy;
    }

    public LocalDate getSubmittedOn() {
        return submittedOn;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<RecipeItem> getInstructions() {
        return instructions;
    }

    public List<RecipeItem> getIngredients() {
        return ingredients;
    }

    public String getDescriptionSummary() {
        return description.length() > SUMMARY_LIMIT ? description.substring(0, SUMMARY_LIMIT) : description;
    }

    @Override
    public Builder builder() {
        return new Builder(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Recipe recipe = (Recipe) o;
        return Objects.equals(id, recipe.id) &&
                Objects.equals(submitterId, recipe.submitterId) &&
                Objects.equals(groupId, recipe.groupId) &&
                Objects.equals(originallyBy, recipe.originallyBy) &&
                Objects.equals(submittedOn, recipe.submittedOn) &&
                Objects.equals(name, recipe.name) &&
                Objects.equals(description, recipe.description) &&
                Objects.equals(instructions, recipe.instructions) &&
                Objects.equals(ingredients, recipe.ingredients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, submitterId, groupId, originallyBy, submittedOn, name, description, instructions, ingredients);
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", submitterId=" + submitterId +
                ", groupId=" + groupId +
                ", originallyBy='" + originallyBy + '\'' +
                ", submittedOn=" + submittedOn +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", instructions=" + instructions +
                ", ingredients=" + ingredients +
                '}';
    }

    public static class Builder implements com.dbickler.recipes.db.models.Builder<Recipe> {
        private Long id;
        private Long submitterId;
        private Long groupId;
        private String originallyBy;
        private LocalDate submittedOn;
        private String name;
        private String description;
        private List<RecipeItem> instructions;
        private List<RecipeItem> ingredients;

        public Builder(Recipe recipe) {
            id = recipe.getId();
            submitterId = recipe.getSubmitterId();
            groupId = recipe.getGroupId();
            originallyBy = recipe.getOriginallyBy();
            submittedOn = recipe.getSubmittedOn();
            name = recipe.getName();
            description = recipe.getDescription();
            instructions = recipe.getInstructions();
            ingredients = recipe.getIngredients();
        }

        public Long getId() {
            return id;
        }

        @Override
        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Long getSubmitterId() {
            return submitterId;
        }

        public Builder withSubmitterId(Long submitterId) {
            this.submitterId = submitterId;
            return this;
        }

        public Long getGroupId() {
            return groupId;
        }

        public Builder withGroupId(Long groupId) {
            this.groupId = groupId;
            return this;
        }

        public String getOriginallyBy() {
            return originallyBy;
        }

        public Builder withOriginallyBy(String originallyBy) {
            this.originallyBy = originallyBy;
            return this;
        }

        public LocalDate getSubmittedOn() {
            return submittedOn;
        }

        public Builder withSubmittedOn(LocalDate submittedOn) {
            this.submittedOn = submittedOn;
            return this;
        }

        public String getName() {
            return name;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public String getDescription() {
            return description;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public List<RecipeItem> getInstructions() {
            return instructions;
        }

        public Builder withInstructions(List<RecipeItem> instructions) {
            this.instructions = instructions;
            return this;
        }

        public List<RecipeItem> getIngredients() {
            return ingredients;
        }

        public Builder withIngredients(List<RecipeItem> ingredients) {
            this.ingredients = ingredients;
            return this;
        }

        @Override
        public Recipe build() {
            return new Recipe(id, submitterId, groupId, originallyBy, submittedOn, name, description, instructions, ingredients);
        }
    }
}
