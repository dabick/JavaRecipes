package com.dbickler.recipes.recipe.models;

import com.dbickler.recipes.translation.ListTranslator;

import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author Daniel Bickler
 */
public class RecipeModel {
    private Long id;
    private Long submitterId;
    private Long groupId;
    private String originallyBy;
    private String submittedOn;
    private String name;
    private String description;
    private List<RecipeItemModel> instructions;
    private List<RecipeItemModel> ingredients;

    @Deprecated
    public RecipeModel() {}

    public RecipeModel(Recipe recipe) {
        id = recipe.getId();
        submitterId = recipe.getSubmitterId();
        groupId = recipe.getGroupId();
        originallyBy = recipe.getOriginallyBy();
        submittedOn = recipe.getSubmittedOn().format(DateTimeFormatter.ISO_DATE);
        name = recipe.getName();
        description = recipe.getDescription();
        instructions = ListTranslator.fromJpa(recipe.getInstructions(), RecipeItemModel::new);
        ingredients = ListTranslator.fromJpa(recipe.getIngredients(), RecipeItemModel::new);
    }

    public Long getId() {
        return id;
    }

    public Long getSubmitterId() {
        return submitterId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public String getOriginallyBy() {
        return originallyBy;
    }

    public String getSubmittedOn() {
        return submittedOn;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<RecipeItemModel> getInstructions() {
        return instructions;
    }

    public List<RecipeItemModel> getIngredients() {
        return ingredients;
    }
}
