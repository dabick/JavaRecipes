package com.dbickler.recipes.recipe.models;

/**
 * @author Daniel Bickler
 */
public class RecipeItemModel {
    private Long recipeId;
    private String name;

    @Deprecated public RecipeItemModel() {} //For JAX-RS

    public RecipeItemModel(RecipeItem item) {
        recipeId = item.getRecipeId();
        name = item.getName();
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public String getName() {
        return name;
    }
}
