package com.dbickler.recipes.recipe;

import com.dbickler.recipes.recipe.models.Recipe;
import com.dbickler.recipes.recipe.models.RecipeModel;
import com.dbickler.recipes.security.Secured;
import com.dbickler.recipes.security.session.SessionUtil;
import com.dbickler.recipes.translation.ListTranslator;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Daniel Bickler
 */
@Path("/recipes")
public class RecipeService {
    private RecipeEJB recipeEJB;
    private SessionUtil sessionUtil;

    @Inject
    @SuppressWarnings("unused")
    void init(RecipeEJB recipeEJB, SessionUtil sessionUtil) {
        this.recipeEJB = recipeEJB;
        this.sessionUtil = sessionUtil;
    }

    @GET
    @Path("/")
    public List<RecipeModel> getRecipes() {
        return ListTranslator.fromJpa(recipeEJB.getRecipes(), RecipeModel::new);
    }

    @POST
    @Secured
    @Path("/")
    public RecipeModel createRecipe(RecipeModel recipeModel, @Context HttpServletRequest request) {
        var recipe = new Recipe(recipeModel).builder()
                .withSubmittedOn(LocalDate.now())
                .withSubmitterId(sessionUtil.getUser(request).getId())
                .build();
        return new RecipeModel(recipeEJB.addRecipe(recipe));
    }
}
