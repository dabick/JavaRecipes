package com.dbickler.recipes.recipe;

import com.dbickler.recipes.db.Parameter;
import com.dbickler.recipes.db.ParameterBuilder;
import com.dbickler.recipes.db.models.DbModel;
import com.dbickler.recipes.recipe.models.Recipe;

import java.util.List;

/**
 * @author Daniel Bickler
 */
public class RecipeDb implements DbModel<Recipe> {
    private final Recipe recipe;

    RecipeDb(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public String getInsertSql() {
        return "INSERT INTO recipes (submitter_id, originally_by, name, description, submitted_on) VALUES (?, ?, ?, ?, ?) RETURNING id";
    }

    @Override
    public List<Parameter<?>> getParametersForInsert() {
        return ParameterBuilder.create()
                .addLong(recipe.getSubmitterId())
                .addString(recipe.getOriginallyBy())
                .addString(recipe.getName())
                .addString(recipe.getDescription())
                .addDate(recipe.getSubmittedOn())
                .build();
    }

    @Override
    public String getUpdateSql() {
        return null;
    }

    @Override
    public List<Parameter<?>> getParametersForUpdate() {
        return null;
    }

    @Override
    public Recipe data() {
        return recipe;
    }
}
