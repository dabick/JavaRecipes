package com.dbickler.recipes.recipe;

import com.dbickler.recipes.db.DaoAdapter;
import com.dbickler.recipes.db.Parameter;
import com.dbickler.recipes.db.ParameterBuilder;
import com.dbickler.recipes.recipe.models.Recipe;
import com.dbickler.recipes.recipe.models.RecipeItem;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Daniel Bickler
 */
@LocalBean
@Stateless
public class RecipeEJB {
    private static final String GET_SQL =
            " SELECT id, submitter_id, group_id, originally_by, submitted_on, name, description " +
            " FROM recipes " +
            " ORDER BY id " +
            " LIMIT 10 ";
    private static final String ITEM_INSERT_SQL =
            " INSERT INTO %s (recipe_id, sequence, name) VALUES %s";

    private static final String INGREDIENT_TABLE_NAME = "ingredients";

    private final DaoAdapter daoAdapter;

    @Inject
    RecipeEJB(DaoAdapter daoAdapter) {
        this.daoAdapter = daoAdapter;
    }

    public List<Recipe> getRecipes() {
        return daoAdapter.getAny(GET_SQL, new ArrayList<>(), Recipe::new);
    }

    public Recipe addRecipe(Recipe recipe) {
        Recipe created = daoAdapter.insert(new RecipeDb(recipe));
        insertRecipeItems(setSequence(recipe.getIngredients()), INGREDIENT_TABLE_NAME, created.getId());
        return created;
    }

    private void insertRecipeItems(List<RecipeItem> items, String tableName, Long recipeId) {
        String inserts = String.join(",", Collections.nCopies(items.size(), "(?,?,?)"));
        var builder = ParameterBuilder.create();
        for (var item : items) {
            builder.addLong(recipeId).addShort(item.getSequence()).addString(item.getName());
        }
        daoAdapter.insert(String.format(ITEM_INSERT_SQL, tableName, inserts), builder.build());
    }

    // The order ingredients and instructions are in when received from the client is assumed to be the desired order.
    private List<RecipeItem> setSequence(List<RecipeItem> original) {
        List<RecipeItem> toReturn = new ArrayList<>(original.size());
        short i = 1;
        for (var item : original) {
            toReturn.add(item.builder().withSequence(i++).build());
        }
        return toReturn;
    }
}
