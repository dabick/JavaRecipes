package com.dbickler.recipes.functional;

/**
 * @author Daniel Bickler
 */
@FunctionalInterface
public interface ThrowingFunction<T, R> {
    R apply(T t) throws Exception;
}
