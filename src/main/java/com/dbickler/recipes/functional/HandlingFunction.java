package com.dbickler.recipes.functional;

/**
 * @author Daniel Bickler
 */
@FunctionalInterface
public interface HandlingFunction<T, F> {
    default F apply(T t) {
        try {
            return this.doApply(t);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    F doApply(T t) throws Exception;
}
