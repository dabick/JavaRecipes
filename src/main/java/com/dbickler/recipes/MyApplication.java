package com.dbickler.recipes;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Daniel Bickler
 */
@ApplicationPath("/api")
public class MyApplication extends Application {}
