package com.dbickler.recipes.properties;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Daniel Bickler
 */
public class OurProperties {
    private final Properties properties = new Properties();

    @Inject
    OurProperties() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("recipes.properties");
        try {
            properties.load(input);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getEnvironment() {
        return properties.getProperty("ENVIRONMENT");
    }

    public boolean isDev() {
        return getEnvironment().equalsIgnoreCase("dev");
    }
}
