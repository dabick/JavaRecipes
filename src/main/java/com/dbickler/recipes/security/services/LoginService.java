package com.dbickler.recipes.security.services;

import com.dbickler.recipes.errors.exceptions.UnauthorizedException;
import com.dbickler.recipes.errors.exceptions.ValidationException;
import com.dbickler.recipes.security.session.SessionUtil;
import com.dbickler.recipes.user.UserEJB;
import com.dbickler.recipes.user.models.json.UserModel;
import com.google.common.base.Strings;
import com.goterl.lazycode.lazysodium.LazySodiumJava;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * @author Daniel Bickler
 */
@Path("/login")
@Consumes(MediaType.APPLICATION_JSON)
public class LoginService {
    @Inject
    private UserEJB userEJB;
    @Inject
    private LazySodiumJava lazySodium;
    @Inject
    private SessionUtil sessionUtil;

    @POST
    public UserModel login(LoginRequest request, @Context HttpServletRequest httpRequest) {
        if (Strings.isNullOrEmpty(request.getEmail()) || Strings.isNullOrEmpty(request.getPassword())) {
            throw new ValidationException("Email and Password are required.");
        }
        try {
            return userEJB.getUserForEmail(request.getEmail())
                    .map(user -> {
                        if (lazySodium.cryptoPwHashStrVerify(user.getPassword(), request.getPassword())) {
                            sessionUtil.login(httpRequest, user);
                            return new UserModel(user);
                        }
                        return null;
                    }).orElseThrow(UnauthorizedException::new);
        } catch (UnauthorizedException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
