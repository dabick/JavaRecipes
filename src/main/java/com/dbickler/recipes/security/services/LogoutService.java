package com.dbickler.recipes.security.services;

import com.dbickler.recipes.security.Secured;
import com.dbickler.recipes.security.session.SessionUtil;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

/**
 * @author Daniel Bickler
 */
@Secured
@Path("/logout")
public class LogoutService {
    @Inject
    private SessionUtil sessionUtil;

    @POST
    public void logout(@Context HttpServletRequest request) {
        sessionUtil.logout(request);
    }
}
