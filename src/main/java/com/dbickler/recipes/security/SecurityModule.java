package com.dbickler.recipes.security;

import com.goterl.lazycode.lazysodium.LazySodiumJava;
import com.goterl.lazycode.lazysodium.SodiumJava;

import javax.enterprise.inject.Produces;
import java.nio.charset.StandardCharsets;

/**
 * @author Daniel Bickler
 */
public class SecurityModule {
    private static final SodiumJava sodium = new SodiumJava();
    private static final LazySodiumJava lazySodium = new LazySodiumJava(sodium, StandardCharsets.UTF_8);

    @Produces
    LazySodiumJava produceLazySodium() {
        return lazySodium;
    }
}
