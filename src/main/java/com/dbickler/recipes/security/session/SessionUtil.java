package com.dbickler.recipes.security.session;

import com.dbickler.recipes.user.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Daniel Bickler
 */
public class SessionUtil {
    public void login(HttpServletRequest request, User user) {
        if (request.getSession(false) != null) {
            request.getSession().invalidate();
        }
        HttpSession session = request.getSession(true);
        session.setAttribute(SessionConstants.USER.code, user);
    }

    /**
     * Gets the logged in user for the current session.
     *
     * NOTE: This does NOT create a session or validate whether the user is set.
     * It is up to the caller to only call this from @Secured services.
     *
     * @param httpRequest to use to retrieve the User
     * @return logged in user.
     *         Please note that the information contained within could be stale,
     *         especially if the user is logged in across devices.
     */
    public User getUser(HttpServletRequest httpRequest) {
        return (User) httpRequest.getSession(false).getAttribute(SessionConstants.USER.code);
    }

    public void logout(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
    }
}
