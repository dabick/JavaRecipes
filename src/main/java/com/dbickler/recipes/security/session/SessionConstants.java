package com.dbickler.recipes.security.session;

/**
 * @author Daniel Bickler
 */
public enum SessionConstants {
    USER("user");

    public final String code;

    SessionConstants(String code) {
        this.code = code;
    }
}
