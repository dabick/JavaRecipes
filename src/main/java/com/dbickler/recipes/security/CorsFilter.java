package com.dbickler.recipes.security;

import com.dbickler.recipes.properties.OurProperties;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * This is currently disabled because I am planning on hosting the website from here.
 * @author Daniel Bickler
 */
@Provider
@PreMatching
public class CorsFilter implements ContainerResponseFilter {
    private final OurProperties ourProperties;

    @Inject
    CorsFilter(OurProperties ourProperties) {
        this.ourProperties = ourProperties;
    }

    @Override
    public void filter(final ContainerRequestContext requestContext,
                       final ContainerResponseContext cres) throws IOException {
        if (ourProperties.isDev() && requestContext.getUriInfo().getBaseUri().getPath().equalsIgnoreCase("/api/")) {
            cres.getHeaders().add("Access-Control-Allow-Origin", "http://localhost:8080");
            cres.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept");
            cres.getHeaders().add("Access-Control-Allow-Credentials", "true");
            cres.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            cres.getHeaders().add("Access-Control-Max-Age", "1209600");
        }
    }

}