package com.dbickler.recipes.security;

import com.dbickler.recipes.security.session.SessionConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * @author Daniel Bickler
 */
@Secured
@Provider
public class SecurityFilter implements ContainerRequestFilter {
    @Context
    private HttpServletRequest servletRequest;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
        HttpSession session = servletRequest.getSession(false);
        if (session == null || session.getAttribute(SessionConstants.USER.code) == null) {
            containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
