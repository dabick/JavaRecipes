package com.dbickler.recipes.models;

/**
 * @author Daniel Bickler
 */
public interface HasId {
    Long getId();
}
