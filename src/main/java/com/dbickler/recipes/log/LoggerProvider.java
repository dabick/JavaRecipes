package com.dbickler.recipes.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Util to "inject" loggers for unit testing.
 *
 * Created by Daniel Bickler
 */
public class LoggerProvider {
    @Inject
    LoggerProvider() {}

    public Logger getLogger(Class clazz) {
        return LoggerFactory.getLogger(clazz);
    }
}