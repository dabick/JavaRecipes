 id | submitterid |              name              |                                                                                                       description                                                                                                        
----+-------------+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  6 |           1 | Venison Steak Marinade         | http://www.food.com/recipe/venison-steak-marinade-119158
  5 |           1 | Duck Breast a l'Orange         | From http://www.marthastewart.com/868606/duck-breast-lorange                                                                                                                                                            +
    |             |                                |                                                                                                                                                                                                                         +
    |             |                                | 
  9 |           1 | Oatmeal Chocolate Chip Cookies | From Deb Kemp
 10 |           1 | Fruity-Tootie Smoothie         | smoothie
 11 |           1 | Very Cherry Smoothie           | Smooth
 12 |           1 | Soft No-Yeast breadsticks      | Quick and easy to make bread sticks for those times when you want homemade bread sticks, but do not have the time to wait for them to rise. This recipe was found at www.domesticgoddess.com and modified to our liking.
 13 |           1 | Chicken Biscuit Bake           | This is for an 8x8 pan, double for 9 x 13
 14 |           1 | Pie Crust                      | Tasty Pie Crust
 15 |           1 | Apple Pie                      | Tasty Apple Pie
 16 |           1 | Sandwich Bread                 | Easy white bread, makes 2 small loaves or 1 large loaf
 17 |           1 | Angel Sugar Cookies            | Tasty Sugar Cookies                                                                                                                                                                                                     +
    |             |                                |                                                                                                                                                                                                                         +
    |             |                                | We make the cookies different every time so the ingredients are a rough approximation. Making the cookies differently has yielded different results.
 18 |           1 | Taco Seasoning                 | Tasty Taco Seasoning for 1lb of hamburger.
 19 |           1 | Tasty buns                     | Yum!
(13 rows)

