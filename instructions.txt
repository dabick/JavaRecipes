 id |                                                                              details                                                                              | recipeid 
----+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------
  0 | Mix all marinade ingredients together in a small measuring cup.                                                                                                   |        6
  1 | Place venison steaks in a large zip lock bag.                                                                                                                     |        6
  2 | Pour marinade over steaks and seal bag.                                                                                                                           |        6
  3 | Place bag in a flat casserole dish so that the steaks are in a single layer.                                                                                      |        6
  4 | Refrigerate and marinate at least 4 hours, turning every half hour to marinate each side.                                                                         |        6
  5 | Drain marinade and grill steaks to desired doneness.                                                                                                              |        6
  6 | I double or triple this recipe for each addition package of steak. Any wild game steak can be used in this recipe.                                                |        6
  0 | Place sugar in a medium saucepan and set over medium-high heat.                                                                                                   |        5
  1 | As sugar begins to melt, use a fork to gently stir sugar from edges to center of pan.                                                                             |        5
  2 | Continue stirring in this manner until sugar is a deep amber color.                                                                                               |        5
  3 | Remove caramel from heat and carefully stir in vinegar, orange zest and juice, chicken broth, and shallot.                                                        |        5
  4 | Return to medium-high heat and boil, stirring occasionally, until reduced to 2/3 cup, 20 to 25 minutes.                                                           |        5
  5 | Score fat of duck breasts with the tip of a knife in a crosshatch pattern to form 1-inch diamonds.                                                                |        5
  6 | Season both sides of breasts with salt and pepper.                                                                                                                |        5
  7 | Place breasts, fat side down, in a medium skillet and place over medium-high heat.                                                                                |        5
  8 | Cook undisturbed until skin is crisp and golden brown, about 10 minutes.                                                                                          |        5
  9 | Flip and continue cooking to desired doneness, about 8 minutes more for medium-rare (125 degrees).                                                                |        5
 10 | Allow meat to rest 10 minutes before slicing and serving.                                                                                                         |        5
 11 | Add orange supremes to sauce and pour over sliced duck just before serving.                                                                                       |        5
  0 | Cream together butter, shortening, brown sugar, and white sugar.                                                                                                  |        9
  1 | Add eggs and vanilla and beat 3-4 minutes                                                                                                                         |        9
  2 | Add remaining ingredients.                                                                                                                                        |        9
  3 | Stir.                                                                                                                                                             |        9
  4 | Bake at 350 degrees for 12 minutes.                                                                                                                               |        9
  0 | Smooth together                                                                                                                                                   |       10
  0 | Smooth together                                                                                                                                                   |       11
  0 | Combine dry ingredients and gradually add milk to form a soft dough.                                                                                              |       12
  1 | Knead gently 3 or 4 times on a floured surface.                                                                                                                   |       12
  2 | Roll out into a rectangle about 10 x 5 inches and cut into 12 sticks or shape into bread sticks with your hands.                                                  |       12
  3 | Put the melted butter into a large baking dish (I use a 9 x 13 inch; rectangular glass dish)                                                                      |       12
  4 | Turn the breadsticks to coat them with the butter or spray with butter flavored non-stick spray for a healthy alternative to butter.                              |       12
  5 | Sprinkle with sesame seeds, Italian seasoning and/or garlic salt, if you like.                                                                                    |       12
  6 | Bake at 450°F for 14 to 18 minutes.                                                                                                                               |       12
  7 | Serve warm.                                                                                                                                                       |       12
  0 | In pan, melt butter                                                                                                                                               |       13
  1 | Blend in flour and salt                                                                                                                                           |       13
  2 | Add broth &amp;amp; milk all at once                                                                                                                              |       13
  3 | Cook, stirring constantly, till sauce is thick and bubbly                                                                                                         |       13
  4 | Add meat &amp;amp; beans                                                                                                                                          |       13
  5 | Heat Through                                                                                                                                                      |       13
  6 | Put in pan                                                                                                                                                        |       13
  7 |                                                                                                                                                                   |       13
  8 | Biscuits                                                                                                                                                          |       13
  9 | Mix first 3 ingredients together                                                                                                                                  |       13
 10 | Cut shortening in finely                                                                                                                                          |       13
 11 | Stir in milk                                                                                                                                                      |       13
 12 | Dough should be soft &amp;amp; easy to roll out                                                                                                                   |       13
 13 | Round up on lightly floured board                                                                                                                                 |       13
 14 | Knead lightly                                                                                                                                                     |       13
 15 | Roll or pat out about 1/2 inch thick                                                                                                                              |       13
 16 | Cut to fit pan                                                                                                                                                    |       13
 17 | Cut extra dough into biscuits                                                                                                                                     |       13
 18 | Place on un-greased sheet                                                                                                                                         |       13
 19 | Bake at 450 degrees for 10-12 minutes for biscuits, about 20 minutes for pan                                                                                      |       13
  0 | Stir flour and salt together.                                                                                                                                     |       14
  1 | Cut in half of lard till like cornmeal, then cut in the rest of the lard until pieces are the size of small peas.                                                 |       14
  2 | Sprinkle one Tbsp. water over part of the mixture.                                                                                                                |       14
  3 | Gently toss with fork; push to side of bowl.                                                                                                                      |       14
  4 | Repeat until all is moistened.                                                                                                                                    |       14
  5 | Form into a ball, for double crust divide into top and bottom portions and form into balls.                                                                       |       14
  6 | Flatten on floured surface.                                                                                                                                       |       14
  7 | Roll out until 1/8&amp;#34; thick.                                                                                                                                |       14
  8 |                                                                                                                                                                   |       14
  9 | To bake single crust pie:                                                                                                                                         |       14
 10 | Fit pastry into pie plate                                                                                                                                         |       14
 11 | trim to 1/2-1 inch beyond edge                                                                                                                                    |       14
 12 | fold under and flute edge by pressing dough with forefinger against wedge made finger and thumb of other hand                                                     |       14
 13 | Prick bottom and sides well with fork                                                                                                                             |       14
 14 | Bake at 450° for 10-12 minutes or until golden brown                                                                                                              |       14
  0 | Combine sugar, flour, spices and dash of salt; mix with apples                                                                                                    |       15
  1 | Line pie pan with pastry                                                                                                                                          |       15
  2 | Fill with apple mixture, dot with butter                                                                                                                          |       15
  3 | Adjust top crust, cutting slits for steam to escape                                                                                                               |       15
  4 | Seal                                                                                                                                                              |       15
  5 | Bake at 400° for 50 minutes or until done.                                                                                                                        |       15
  0 | Add sugar and yeast to warm water, stir, let sit.                                                                                                                 |       16
  1 | In a sauce pan, heat butter and milk until warm and combined. Add salt. Add ~ 1/4 cup flour to milk mixture and let sit (optional, I feel this improves texture). |       16
  2 | Measure 2 cups flour into large bowl and add yeast mixture. Stir until combined, then add milk-flour mixture.                                                     |       16
  3 | Stir slowly, adding more flour a cup at a time until a ball forms.                                                                                                |       16
  4 | Turn out onto a floured surface and knead, adding flour until stiff dough forms (not too stiff though, it should still flex easily).                              |       16
  5 | Pour small amount of oil into mixing bowl, spread up sides,                                                                                                       |       16
  6 | put ball of dough into bowl, turn so oil coated side is on top, cover,                                                                                            |       16
  7 | let rise in a warm environment until doubled (when poked, the surface shouldn&amp;#39;t bounce back), about 1/2 to 1 hour.                                        |       16
  8 | Punch dough down, a quick knead is optional (I skip this step, but often end up with large holes inside my bread).                                                |       16
  9 | Cut in half, shape into load and place in oiled loaf pans, 8x4 inches. (Or one 9x5, will make a really large loaf).                                               |       16
 10 | Let rise for another 1/2 to 1 hour.                                                                                                                               |       16
 11 | Preheat oven to 400 degrees F.                                                                                                                                    |       16
 12 | Place loaves in oven, check at 15 minute mark, rotate if needed.                                                                                                  |       16
 13 | Tap loaves with wooded spoon at 30 minutes.                                                                                                                       |       16
 14 | They are done if they sound hollow. (If making 1 giant loaf, reduce heat to 350 degrees at 15 minutes, bake 35-45 minutes).                                       |       16
 15 | Let rest in pans ~ 10 minutes, remove. Take stick of butter and run over warm tops of loaves, cover with towel and allow to cool.                                 |       16
  0 | Melt butter                                                                                                                                                       |       17
  1 | Add and mix in wet ingredients                                                                                                                                    |       17
  2 | In separate bowl, mix dry ingredients                                                                                                                             |       17
  3 | Mix wet and dry ingredients                                                                                                                                       |       17
  4 | Preheat oven to 350 degrees                                                                                                                                       |       17
  5 | Dip balls in water and then sugar                                                                                                                                 |       17
  6 | Bake 10-15 minutes                                                                                                                                                |       17
  7 |                                                                                                                                                                   |       17
  0 | Mix together ingredients. Store in an air tight container or use. Recipe is for 1lb of hamburger.                                                                 |       18
  0 | Yeast water sugar oil                                                                                                                                             |       19
  1 | Egg salt                                                                                                                                                          |       19
  2 | Flour                                                                                                                                                             |       19
  3 | Knead                                                                                                                                                             |       19
  4 | Tear into 12 balls                                                                                                                                                |       19
  5 | Let rest at least 10 min                                                                                                                                          |       19
  6 | Bake at 450 until done (about 12 min)                                                                                                                             |       19
(107 rows)

